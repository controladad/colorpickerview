# README #

Clone and improved from : https://bitbucket.org/fribell/colorpickerview

[![Android Arsenal](https://img.shields.io/badge/Android%20Arsenal-ColorPickerView-green.svg?style=flat)](https://android-arsenal.com/details/1/2389)

ColorPickerView is an android custom view, you can pick one colour from a gridview. You can define the columns, colours, border colour, ...

![colorpickerview.png](https://bitbucket.org/repo/o6rkXe/images/1988054770-colorpickerview.png)
### Download ###

```
#!gradle
dependencies {
    compile(group: 'com.ribell', name: 'colorpickerview', version: '1.0.1', ext: 'aar')
}
```

### Basic Usage ###

Add the com.ribell.colorpickerview.ColorPickerView to your layout XML file.

```
#!xml
<com.ribell.colorpickerview.ColorPickerView
                android:id="@+id/gridview"
                android:layout_margin="@dimen/spacing_xlarge"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:columnWidth="90dp"
                android:numColumns="auto_fit"
                android:verticalSpacing="10dp"
                android:horizontalSpacing="10dp"
                android:stretchMode="columnWidth"
                android:gravity="center"
                android:layout_gravity="center"/>

```

Add the list of colours in array.xml and set it in the layout XML:
```
#!xml
<array name="rainbow">
        <item>@color/red_300</item>
        <item>@color/purple_300</item>
        <item>@color/blue_grey_300</item>
        <item>@color/brown_300</item>        
</array>
```

```
#!xml
android:entries="@array/rainbow"
```

Set list of colours programmatically
```
#!java
ArrayList<Integer> colorArrayList = new ArrayList<>();
colorArrayList.add(getResources().getColor(R.color.red_300));
colorArrayList.add(getResources().getColor(R.color.blue_300));
colorArrayList.add(getResources().getColor(R.color.brown_300));
colorArrayList.add(getResources().getColor(R.color.lime_300));
colorArrayList.add(getResources().getColor(R.color.orange_300));
colorArrayList.add(getResources().getColor(R.color.teal_300));
colorPickerView.setColorsList(colorArrayList);
```


### Customisation ###

Set default border color and the selected border in the layout XML:
>**NOTE:** Add XML namespace prefix: xmlns:custom="http://schemas.android.com/apk/res-auto"


```
#!xml
custom:borderColorSelected="@color/accent"
custom:borderColor="@color/grey_60alpha"

```

Set it programmatically
```
#!java
colorPickerView.setBorderColor(getResources().getColor(R.color.grey_60alpha));
colorPickerView.setBorderColorSelected(getResources().getColor(R.color.accent))
```

### Handle click ###

You have to implement ColorPickerViewListener
```
#!java
public class MeetingActivity extends AppCompatActivity implements ColorPickerViewListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ColorPickerView colorPickerView = (ColorPickerView) findViewById(R.id.gridview);
        colorPickerView.setListener(this);
    }
    @Override
    public void onColorPickerClick(int colorPosition) {
        Log.d(TAG, "TEST color: "+colorPosition);
    }
}
```

### XML Attributes ###
XML sample here.

```xml
    <com.ribell.colorpickerview.ColorPickerView
                android:id="@+id/gridview"
                android:layout_margin="@dimen/spacing_xlarge"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:columnWidth="90dp"
                android:numColumns="auto_fit"
                android:verticalSpacing="10dp"
                android:horizontalSpacing="10dp"
                android:stretchMode="columnWidth"
                android:gravity="center"
                android:layout_gravity="center"
                custom:borderColorSelected="@color/accent"
                custom:borderColor="@color/grey_60alpha"
                android:entries="@array/rainbow"/>
```

| XML Attribute | Related Method | Description |
|:---|:---|:---|
| borderColorSelected | setBorderColorSelected(int resId) | Set border colour when we select a colour. |
| borderColor | setBorderColor(int resId) | Set border color. |
| entries | setColorsList(ArrayList<Integer> colorArrayList) | Set the list of colours. |

### Developed By ###
 * Ferran Ribell - <fribell@gmail.com>

### License ###

```
Copyright 2015 Ferran Ribell (ColorPickerView)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
